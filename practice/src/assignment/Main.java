package assignment;

import java.util.Scanner;

public class Main
{
   public static void main(String[] args) 
   {
     while (true)
     {
        Scanner abc = new Scanner(System.in);
        System.out.println(  "Enter 1 for Super department. "+
                             "Enter 2 for Admin department. "+
                             "Enter 3 for HR department. "   +
                             "Enter 4 for Tech department. "+
                             "Enter 5 for exit");
        int m=abc.nextInt();
        switch(m)
        {
          case 1:
          SuperDept pri = new SuperDept();
          System.out.println(pri.depName());
          System.out.println(pri.getTodaysWork());
          System.out.println(pri.getWorkDeadline());
          System.out.println(pri.isTodayAHoliday());
          break;
          case 2 :
          AdminDept pri1 = new AdminDept();
          System.out.println(pri1.depName());
          System.out.println(pri1.getTodaysWork());
          System.out.println(pri1.getWorkDeadline());
          break;
          case 3 :
          HrDeptt pri2 = new HrDeptt();
          System.out.println(pri2.depName());
          System.out.println(pri2.getTodaysWork());
          System.out.println(pri2.doactivity());
          System.out.println(pri2.getWorkDeadline());
          break;
          case 4:
          TechDeptt pri3 = new TechDeptt();
          System.out.println(pri3.depName());
          System.out.println(pri3.getTodaysWork());
          System.out.println(pri3.getTechStackInformation());
          System.out.println(pri3.getWorkDeadline());
          break;
          case 5:
          System.exit(1);
         
          default:
          System.out.println("Enter invalid option");
         }
    }
 }
}

