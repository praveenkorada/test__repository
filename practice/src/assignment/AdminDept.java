package assignment;

public class AdminDept extends SuperDept
{
	
	//declare method departmentName of return type string
	public String depName()
	{
	  return "ADMIN DEPARTMENT";
	}

	//declare method getTodaysWork of return type string
	public String getTodaysWork()
	{
	  return "COMPLETE YOUR DOCUMENTS SUBMISSION";
	}

	//declare method getWorkDeadline of return type string
	public String getWorkDeadline() 
	{
	  return "COMPLETE BY END OF THE DAY ";
	}

}